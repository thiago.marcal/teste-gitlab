import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'Site_PI',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
