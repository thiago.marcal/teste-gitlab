Treino A: Peito/Triceps
- Supino Halter inclinado 4x falha (progressao de carga)
- Crucifuxo Polia Baixa 3x12 
- Supino Máquina 4x12 

- Triceps Testa Barra W 3x12
- Triceps Corda 4x falha
- Triceps Frances 3x12

Treino B: Costas/Biceps
- Serrote 3x12 (progressao de carga)
- Pulldown 4x12 
- Remada 4x12 

- Banco Inclinado 45 graus 3x12
- Martelo 4x12
- Barra W 4x10

Treino C: Perna
- Leg Press 4x15
- Agachamento Smith 4x10
- Bulgaro 3x15
- Extensora 4x10
- Cadeira Flexora 3x12
- Mesa Flexora 4x12
- Panturrilha 4x falha


